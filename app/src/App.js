import "./App.css";
import SelectData from "./components/data/SelectData";
import DataProvider from "./components/dataProvider/DataProvider";

function App() {
  return (
    <div className="App">
      <DataProvider>
        <SelectData />
      </DataProvider>
    </div>
  );
}

export default App;
