export const productsData = [
  {
    id: "A",
    title: "item 1",
    created: "2022-12-03T07:22:25.911Z",
    uniqueId: 1,
    isChecked:false
  },
  {
    id: "A",
    title: "item 2",
    created: "2022-08-15T07:22:25.911Z",
    uniqueId: 2,
    isChecked:false
  },
  {
    id: "A",
    title: "item 3",
    created: "2021-07-28T07:22:25.911Z",
    uniqueId: 3,
    isChecked:false
  },
  {
    id: "A",
    title: "item 4",
    created: "2022-05-15T07:22:25.911Z",
    uniqueId: 4,
    isChecked:false
  },
  {
    id: "A",
    title: "item 5",
    created: "2021-11-07T07:22:25.911Z",
    uniqueId: 5,
    isChecked:false
  },
  {
    id: "B",
    title: "item 6",
    created: "2022-04-10T07:22:25.911Z",
    uniqueId: 6,
    isChecked:false
  },
  {
    id: "B",
    title: "item 7",
    created: "2022-12-03T07:22:25.911Z",
    uniqueId: 7,
    isChecked:false
  },
  {
    id: "B",
    title: "item 8",
    created: "2022-08-15T07:22:25.911Z",
    uniqueId: 8,
    isChecked:false
  },
  {
    id: "C",
    title: "item 9",
    created: "2021-07-28T07:22:25.911Z",
    uniqueId: 9,
    isChecked:false
  },
  {
    id: "C",
    title: "item 10",
    created: "2022-05-15T07:22:25.911Z",
    uniqueId: 10,
    isChecked:false
  },
  {
    id: "C",
    title: "item 11",
    created: "2021-11-07T07:22:25.911Z",
    uniqueId: 11,
    isChecked:false
  },
  {
    id: "C",
    title: "item 12",
    created: "2022-04-10T07:22:25.911Z",
    uniqueId: 12,
    isChecked:false
  },
  {
    id: "C",
    title: "item 13",
    created: "2022-12-03T07:22:25.911Z",
    uniqueId: 13,
    isChecked:false
  },
  {
    id: "D",
    title: "item 14",
    created: "2022-08-15T07:22:25.911Z",
    uniqueId: 14,
    isChecked:false
  },
  {
    id: "D",
    title: "item 15",
    created: "2021-07-28T07:22:25.911Z",
    uniqueId: 15,
    isChecked:false
  },
  {
    id: "D",
    title: "item 16",
    created: "2022-05-15T07:22:25.911Z",
    uniqueId: 16,
    isChecked:false
  },
  {
    id: "D",
    title: "item 17",
    created: "2021-11-07T07:22:25.911Z",
    uniqueId: 17,
    isChecked:false
  },
  {
    id: "E",
    title: "item 18",
    created: "2022-04-10T07:22:25.911Z",
    uniqueId: 18,
    isChecked:false
  },
  {
    id: "E",
    title: "item 19",
    created: "2021-11-07T07:22:25.911Z",
    uniqueId: 19,
    isChecked:false
  },
  {
    id: "E",
    title: "item 20",
    created: "2022-04-10T07:22:25.911Z",
    uniqueId: 20,
    isChecked:false
  },
];
