import { useState } from "react";
import "../App.css";
const AccordionData = ({ title, quantity, children }) => {
  const [isOpen, setOpen] = useState(false);
  return (
    <div className="accordion-wrapper">
      <div
        className={`accordion-title bg-slate-100 py-2 px-4 rounded-md mb-2 cursor-pointer flex justify-between items-center ${
          isOpen ? "open" : ""
        }`}
        onClick={() => setOpen(!isOpen)}
      >
        <p>{title}</p>
        <p>quantity:{quantity.length}</p>
        <input type="checkbox" name="groupA" />
      </div>
      <div className={`accordion-item ${!isOpen ? "collapsed" : ""}`}>
        <div className="accordion-content">{children}</div>
      </div>
    </div>
  );
};

export default AccordionData;
