import { useData, useDataAction } from "../dataProvider/DataProvider";

import Moment from "moment";

import AccordionData from "../AccordionData";

const SelectData = () => {
  const datas = useData();

  const dispatch = useDataAction();

  return (
    <section className=" bg-slate-200 p-4 md:p-8 rounded-md">
      <p className="text-3xl text-center">All Data</p>

      <div className="flex justify-end">
        <button
          className="py-2 px-6 bg-slate-300 rounded-md font-medium mb-6 "
          onClick={() => dispatch({ type: "addData" })}
        >
          Add
        </button>
      </div>
      <AccordionData title="A" quantity={datas.A}>
        <ul>
          {datas.A.map((data, index) => {
            return (
              <li
                key={index}
                className="flex  items-center justify-between mb-2 py-2 px-4 rounded-md  bg-slate-300"
              >
                <span>#{data.uniqueId}</span>
                <span>{Moment(data.created).format("DD/MM/YYYY")}</span>
                <input
                  type="checkbox"
                  onChange={() =>
                    dispatch({ type: "checkboxA", id: data.uniqueId })
                  }
                />
              </li>
            );
          })}
        </ul>
      </AccordionData>

      <AccordionData title="B" quantity={datas.B}>
        <ul>
          {datas.B.map((data, index) => {
            return (
              <li
                key={index}
                className="flex  items-center justify-between mb-2 py-2 px-4 rounded-md  bg-slate-300"
              >
                <span>#{data.uniqueId}</span>
                <span>{Moment(data.created).format("DD/MM/YYYY")}</span>
                <input type="checkbox" />
              </li>
            );
          })}
        </ul>
      </AccordionData>

      <AccordionData title="C" quantity={datas.C}>
        <ul>
          {datas.C.map((data, index) => {
            return (
              <li
                key={index}
                className="flex  items-center justify-between mb-2 py-2 px-4 rounded-md  bg-slate-300"
              >
                <span>#{data.uniqueId}</span>
                <span>{Moment(data.created).format("DD/MM/YYYY")}</span>
                <input type="checkbox" />
              </li>
            );
          })}
        </ul>
      </AccordionData>

      <AccordionData title="D" quantity={datas.D}>
        <ul>
          {datas.D.map((data, index) => {
            return (
              <li
                key={index}
                className="flex  items-center justify-between mb-2 py-2 px-4 rounded-md  bg-slate-300"
              >
                <span>#{data.uniqueId}</span>
                <span>{Moment(data.created).format("DD/MM/YYYY")}</span>
                <input type="checkbox" />
              </li>
            );
          })}
        </ul>
      </AccordionData>

      <AccordionData title="E" quantity={datas.E}>
        <ul>
          {datas.E.map((data, index) => {
            return (
              <li
                key={index}
                className="flex  items-center justify-between mb-2 py-2 px-4 rounded-md  bg-slate-300"
              >
                <span>#{data.uniqueId}</span>
                <span>{Moment(data.created).format("DD/MM/YYYY")}</span>
                <input type="checkbox" />
              </li>
            );
          })}
        </ul>
      </AccordionData>
    </section>
  );
};

export default SelectData;
