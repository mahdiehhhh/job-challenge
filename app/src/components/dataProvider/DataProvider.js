import React, { useContext, useReducer, useState } from "react";
import { productsData } from "../../db/Data";

const dataContext = React.createContext(); //state

const dataContextDispatcher = React.createContext(); //action

var outObject = productsData.reduce(function (id, index) {
  let dataId = index["id"];

  (id[dataId] ? id[dataId] : (id[dataId] = null || [])).push(index);
  return id;
}, {});

const reducer = (state, action) => {
  switch (action.type) {
    case "addData":
      console(action.id);
      return state;
    case "checkboxA":
      console.log("hi", action.id);
      return state;
    default:
      return state;
  }
};
const DataProvider = ({ children }) => {
  const [datas, dispatch] = useReducer(reducer, outObject);
  return (
    <div>
      <dataContext.Provider value={datas}>
        <dataContextDispatcher.Provider value={dispatch}>
          {children}
        </dataContextDispatcher.Provider>
      </dataContext.Provider>
    </div>
  );
};

export default DataProvider;

export const useData = () => useContext(dataContext);

export const useDataAction = () => {
  return useContext(dataContextDispatcher);
};
